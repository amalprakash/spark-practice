package amal.example.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.expressions.UnboundedPreceding
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{asc, col, countDistinct, desc, sum, to_date, weekofyear, year}

object WindowDemo {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("AggApp").master("local[3]").getOrCreate()

    val invoicesDF = spark.read.options(Map("inferSchema"->"true","header"->"true")).csv("data/invoices.csv")

    val weeklySalesDf = invoicesDF.withColumn("InvoiceDate", to_date(col("InvoiceDate"), "dd-MM-yyyy H.mm"))
      .filter(year(col("InvoiceDate")) === 2011)
      .groupBy(col("Country"), year(col("InvoiceDate")).as("Year"), weekofyear(col("InvoiceDate")).as("WeekNumber"))
      .agg(countDistinct("InvoiceNo").as("NumInvoices")
        , sum("Quantity").as("TotalQty")
        , sum(col("Quantity") * col("UnitPrice")).as("InvoiceValue"))
      .orderBy(desc("Country"), desc("Year"), asc("WeekNumber"))
      .select("Country", "Year", "WeekNumber", "NumInvoices", "TotalQty", "InvoiceValue")

    val windowSpec = Window.partitionBy("Country")
      .orderBy("WeekNumber").rowsBetween(Window.unboundedPreceding,Window.currentRow)

    val summaryDF = weeklySalesDf.withColumn("RunningTotal", sum("InvoiceValue").over(windowSpec))

    summaryDF.select("Country", "Year", "WeekNumber", "NumInvoices", "TotalQty", "InvoiceValue","RunningTotal").show(50)
  }
}
