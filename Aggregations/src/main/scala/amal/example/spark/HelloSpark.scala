package amal.example.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object HelloSpark extends Serializable {

  def main(args: Array[String]):Unit = {
    val spark = SparkSession.builder().appName("AggApp").master("local[3]").getOrCreate()

    val invoicesDF = spark.read.options(Map("inferSchema"->"true","header"->"true")).csv("data/invoices.csv")

//    invoicesDF.select(
//      count("*").as("CountAll"),
//      sum("Quantity").as("TotalQty"),
//      avg("UnitPrice") as("AvgPrice"),
//      countDistinct("InvoiceNo").as("TotalInvoices")
//    ).show()

//    invoicesDF.groupBy(col("Country"),col("InvoiceNo"))
//      //.agg("sum(Quantity) as TotalQuantity,sum(Quantity * UnitPrice) as InvoiceValue")
//      .agg(sum("Quantity").as("TotalQty"),sum(col("Quantity") * col("UnitPrice")).as("InvoiceValue"))
//      .select("Country","InvoiceNo","TotalQty","InvoiceValue")
//      .orderBy(desc("InvoiceValue"))
//      .show(20,false)

    invoicesDF.withColumn("InvoiceDate",to_date(col("InvoiceDate"),"dd-MM-yyyy H.mm"))
      .filter(year(col("InvoiceDate")) === 2011)
      .groupBy(col("Country"),year(col("InvoiceDate")).as("Year"),weekofyear(col("InvoiceDate")).as("WeekNumber"))
      .agg(countDistinct("InvoiceNo").as("NumInvoices")
        ,sum("Quantity").as("TotalQty")
        ,sum(col("Quantity") * col("UnitPrice")).as("InvoiceValue"))
      .orderBy(desc("Country"),desc("Year"),asc("WeekNumber"))
      .select("Country","Year","WeekNumber","NumInvoices","TotalQty","InvoiceValue").show(20)


    spark.stop()
  }

}
