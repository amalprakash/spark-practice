package amal.example.spark

import amal.example.spark.RowDemo.toDateDF
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.scalatest.BeforeAndAfterAll

import java.sql.Date

case class MyRecord(id:String,event_date:Date)
class RowDemoTest extends org.scalatest.FunSuite with BeforeAndAfterAll{

  @transient var spark:SparkSession = _
  @transient var df:DataFrame = _

  override protected def beforeAll(): Unit = {
    spark = SparkSession
      .builder()
      .master("local[3]")
      .appName("RowDemoTest")
      .getOrCreate()

    val mySchema = StructType(List(StructField("ID",StringType),StructField("EVENT_DATE",StringType)))
    val myRows = Seq(Row("100","02/03/2020"),Row("200","2/3/2020"),Row("300","02/3/2020"),Row("400","2/03/2020"))
    val myRDD = spark.sparkContext.parallelize(myRows)
    df = spark.createDataFrame(myRDD,mySchema)
  }

  override protected def afterAll(): Unit = {
    spark.close()
  }

  test("Test datatype of fields is Date")  {
    val newDF = toDateDF(df,"EVENT_DATE","M/d/y")
    newDF.collect().foreach( row => assert(row.get(1).isInstanceOf[Date]))
  }

  test("Test date values is 02-03-2020") {
    val newDF = toDateDF(df,"EVENT_DATE","M/d/y")
    val spark2 = spark
    import spark2.implicits._
    val myRecords = newDF.as[MyRecord].collectAsList()
    println(myRecords.toString)
    myRecords.forEach( record => assert(record.event_date.toString.equals("2020-02-03")))
  }

}
