package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions.{col, to_date}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object RowDemo extends Serializable {
  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[3]")
      .appName("RowDemoApp").getOrCreate()
    val mySchema = StructType(List(StructField("ID",StringType,true),StructField("EVENT_DATE",StringType,true)))
    val myData = Seq(Row("100","02/03/2020"),Row("200","2/3/2020"),Row("300","02/3/2020"),Row("400","03/2/2020"))
    val myRDD = spark.sparkContext.parallelize(myData)

    val myDF = spark.createDataFrame(myRDD,mySchema)
    val newDF = toDateDF(myDF, "EVENT_DATE", "M/d/y")
    newDF.show(truncate = false)
    println("Original Schema:"+ myDF.schema.simpleString)
    println("Modified Schema:"+newDF.schema.simpleString)
  }

  def toDateDF(df:DataFrame,fieldName:String,dateFrmt:String):DataFrame = {
    df.withColumn(fieldName,to_date(col(fieldName),dateFrmt))
  }
}