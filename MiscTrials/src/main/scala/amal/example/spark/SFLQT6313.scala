package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions.{col, expr, lit}

object SFLQT6313 {

  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
      val spark = SparkSession.builder()
        .master("local[*]")
        .appName("ComparisonApp")
        .getOrCreate()

    val expectedDF = spark.read.option("header","true").option("inferSchema","true").csv("input/expected.csv")
    val actualDF = spark.read.option("header","true").option("inferSchema","true").csv("input/actual.csv")

    expectedDF.printSchema()
    actualDF.printSchema()

    val joinedDF = expectedDF.join(actualDF,expectedDF.col("contact_id")===actualDF.col("contactId"),"left")
    joinedDF.printSchema()
    val finalResults = joinedDF.withColumn("match_found",col("new_product") === col("prodExternalId"))
    println(s"writing ${finalResults.count()} records to file.")
    finalResults.write.mode(SaveMode.Overwrite)
      .option("header","true")
      .csv("output")
    spark.stop()
}

}
