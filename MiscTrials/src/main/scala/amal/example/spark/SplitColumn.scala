package amal.example.spark

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{ArrayType, IntegerType, StringType, StructType}

object SplitColumn extends Serializable {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("MyApp")
      .getOrCreate()

    //val columns = Seq("name", "address")
    val mySchema = new StructType()
      .add("name", new ArrayType(StringType, true))
      .add("address", new StructType()
        .add("street_address", StringType)
        .add("city", StringType)
        .add("state", StringType)
        .add("zip", IntegerType))

    val data = Seq(Row(Seq("Robert", "Smith"), Row("1 Main st", "Newark", "NJ", 92537)),
      Row(Seq("Maria", "Garcia"), Row("3456 Walnut st", "Newark", "NJ", 94732)))

    val df1 = spark.createDataFrame(spark.sparkContext.parallelize(data), mySchema)

    //    val df2 = df1.withColumn("nameSplit", split(col("name"), ","))
    //      .withColumn("splitAddress", split(col("address"), ","))
    //      .select(col("nameSplit").getItem(0).as("firstName"),col("splitAddress").getItem(0).as("streetAddress")).show()

    //print first name, street name and zip

    //    df1.withColumn("first_name", split(col("name"), ","))
    //      .select(col("first_name").getItem(0)
    //        , col("address.street_address")
    //        , col("address.zip").as("zip_code")).show()

    df1.withColumn("first_name", col("name").getItem(0))
      .withColumn("street", col("address.street_address"))
      .withColumn("zip_code", col("address.zip"))
      .select("first_name", "street", "zip_code")
      .show()
  }

}
