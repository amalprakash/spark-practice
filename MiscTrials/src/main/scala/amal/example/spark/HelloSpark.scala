package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object HelloSpark {

  @transient lazy val logger:Logger = Logger.getLogger(getClass.getName)
  def main(args: Array[String]):Unit = {

    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("MiscApp")
      .getOrCreate()

    val cols = Seq("language","users_count")
    val data = Seq(("Java",20000),("Python",10000),("Scala",3000))

    import spark.implicits._
    val myDF1: DataFrame = spark.sparkContext.parallelize(data).toDF(cols: _*)
    myDF1.printSchema()

    val mySchema = StructType(Seq(StructField("language",StringType,true),
      StructField("users_count",IntegerType,true)))
    val myRows = Seq(Row("Java",20000),Row("Python",10000),Row("Scala",3000))

    var dfFromData3 = spark.createDataFrame(spark.sparkContext.parallelize(myRows),mySchema)
  }

}
