package amal.example.spark

import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions.{col, expr, when}

object CaseWhen extends Serializable {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("MyApp")
      .getOrCreate()

    val data = List(("James","","Smith","36636","M",60000),
      ("Michael","Rose","","40288","Male",70000),
      ("Robert","","Williams","42114","",400000),
      ("Maria","Anne","Jones","39192","F",500000),
      ("Jen","Mary","Brown","","Female",0))

    val cols = Seq("first_name","middle_name","last_name","dob","gender","salary")
    val df = spark.createDataFrame(data).toDF(cols:_*)

//    df.select(col("*"),when(col("gender")==="M" || col("gender")==="Male" ,"Male")
//      .when(col("gender")==="F" || col("gender")==="Female","Female")
//      .otherwise("Unknown")
//      .as("sex")).drop("gender")
//      .show()

//    df.withColumn("gender",when(col("gender")==="M" || col("gender")==="Male" ,"Male")
//      .when(col("gender")==="F" || col("gender")==="Female","Female")
//      .otherwise("Unknown")).show()

    val df2 = df.select(col("*"),
      expr(
        """
          |case when gender = 'F' or gender = 'Female' then 'Female'
          | when gender = 'M' or gender = 'Male' then 'Male'
          |  else 'Unknown' end
          |""".stripMargin).as("sex"))
      .drop("gender")

    df2.write    //.partitionBy("sex","salary")
      .mode(SaveMode.Append)
      .parquet("./output")


  }

}
