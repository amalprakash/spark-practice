package amal.example.spark

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.functions.{col, lit}
import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

object RenameColumn {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("MyApp")
      .getOrCreate()

    val data = Seq(Row(Row("James ","","Smith"),"36636","M",3000),
      Row(Row("Michael ","Rose",""),"40288","M",4000),
      Row(Row("Robert ","","Williams"),"42114","M",4000),
      Row(Row("Maria ","Anne","Jones"),"39192","F",4000),
      Row(Row("Jen","Mary","Brown"),"","F",-1)
    )

    val mySchema = new StructType().add("name",new StructType()
    .add("first_name",StringType)
    .add("middle_name",StringType)
    .add("last_name",StringType))
      .add("dob",StringType)
      .add("gender",StringType)
      .add("salary",IntegerType)

    import spark.implicits._
    val df = spark.createDataFrame(spark.sparkContext.parallelize(data), mySchema)

//    df.withColumnRenamed("gender","sex").show()
    df.withColumn("country",lit("USA")).show()
  }
}
