package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

case class MyLog(ip:String, date:String,request:String, referrer:String)
case class ErrorLog(error:String)
object LogDemo2 {
  @transient lazy val logger:Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("My log parser")
      .master("local[3]")
      .getOrCreate()

    val myRegex = """(^\S+) (\-) (\-) (\[.+\]) \"(\w+) (\S+) (\S+) (\d+) ([\d-]*) (\".*\") (\".*\")$""".r

    val rawLog: DataFrame = spark.read.textFile("data/apache_logs.txt").toDF()

    import spark.implicits._
    val parsedLog = rawLog.map(row => {
      row.getString(0) match {
        case myRegex(ip, client, user, date, cmd, request, protocol, status, bytes, referrer, useragent) => MyLog(ip = ip, date = date, request = request, referrer = referrer)
       }
    })

    import org.apache.spark.sql.functions._
    //parsedLog.show(10,false)
    val referrerDomain = """(.*)(\/\/)([\w\.]+)(.*)"""
    parsedLog.withColumn("referrer",regexp_replace($"referrer","\"",""))
      .filter(col("referrer") =!= "-")
      .withColumn("referrer",regexp_extract($"referrer",referrerDomain,3))
      .select("ip","date","request","referrer")
      .groupBy("referrer")
      .agg(count($"referrer").as("count_referrer"))
      .orderBy(desc("count_referrer"))
      .select($"referrer",$"count_referrer")
      .show(100,false)

  }
}
