package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.{col, desc, substring_index}

import scala.util.matching.Regex

case class LogRecord(ip:String, date:String,request:String, referrer:String)
object LogFileDemo {

  @transient lazy val logger:Logger = Logger.getLogger(getClass.getName)
  def main(args: Array[String]):Unit = {
    val spark = SparkSession
      .builder()
      .master("local[3]")
      .appName("LogfileDemo")
      .getOrCreate()

    import spark.implicits._
    val myRegx = """^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+) (\S+) (\S+)" (\d{3}) (\S+) "(\S+)" "([^"]*)"""".r
    val logDF: DataFrame = spark.read.textFile("data/apache_logs.txt").toDF()

    val logDS= logDF.map(row => {
      row.getString(0) match {
        case myRegx(ip, client, user, date, cmd, request, protocol, status, bytes, referrer, useragent) =>
          LogRecord(ip, date, request, referrer)
      }
    })
    //logDS.show(10)
    logDS.where(col("referrer") =!= "-")
      .withColumn("referrer",substring_index(col("referrer"),"/",3))
      .groupBy("referrer").count().as("count").orderBy(desc("count"))
      .show(10,false)





  }

}
