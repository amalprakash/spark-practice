package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.dsl.expressions.StringToAttributeConversionHelper
import org.apache.spark.sql.functions._

object HelloSpark {

  @transient lazy val logger:Logger = Logger.getLogger(getClass.getName)
  def main(args: Array[String]):Unit = {

    val spark = SparkSession
      .builder().appName("SurveyApp").master("local[3]")
      .getOrCreate()

    spark.sparkContext.version

    val surveyDF = spark.read.option("inferSchema","true").option("header","true").csv("https://storage.googleapis.com/amal-public-access-data/24-counting-window/src/main/resources/data/invoices.csv")
//      spark.read.options(Map("inferSchema"->"true","header"->"true"))
//      .csv("data/survey.csv")

    //to use UDF in dataframe
    val parseGenderUDF = udf(parseGender(_: String): String)
    val surveyDFParsed = surveyDF.withColumn("Gender",parseGenderUDF(col("Gender")))

    surveyDFParsed.select(col("Age"),col("Gender"),col("Country"))
            .show(20,false)

    //to use UDF in SQL
    spark.udf.register("parseGenderUDF",parseGender(_:String):String)
    surveyDF.createOrReplaceGlobalTempView("survey_table")
    spark.sql("SELECT Age,Country,parseGenderUDF(Gender) from global_temp.survey_table").show(20,false)

  }

  def parseGender(gender: String):String = {
    val malePattern = "^m$|ma|m.l".r
    val femalePattern = "^f$|f.m|w.m".r

    if (malePattern.findFirstIn(gender.toLowerCase).nonEmpty) "Male"
    else if (femalePattern.findFirstIn(gender.toLowerCase).nonEmpty) "Female"
    else "Unknown"
  }

}
