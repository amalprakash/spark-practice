package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.expr
import org.apache.spark.sql.streaming.{OutputMode, Trigger}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration


object HelloSpark {
@transient lazy val logger: Logger = Logger.getLogger(getClass.getName)
  def main(args: Array[String]):Unit = {

    val spark = SparkSession.builder()
      .appName("FileSourceSink")
      .config("spark.streaming.stopGracefullyOnShutdown","true")
      .config("spark.sql.streaming.schemaInference","true")
      .master("local[3]")
      .getOrCreate()

    val invoiceDF = spark
      .readStream
      .option("path","data")
      .option("maxFilesPerTrigger",1)
      .format("json")
      .load()

    val explodedDF = invoiceDF.selectExpr("InvoiceNumber","CreatedTime","StoreID","PosID","CustomerType","PaymentMethod",
    "DeliveryType","DeliveryAddress.City","DeliveryAddress.State","DeliveryAddress.PinCode","explode(InvoiceLineItems) as LineItem")

    val flatDF = explodedDF.withColumn("ItemCode",expr("LineItem.ItemCode"))
      .withColumn("ItemDescription",expr("LineItem.ItemDescription"))
      .withColumn("ItemPrice",expr("LineItem.ItemPrice"))
      .withColumn("ItemQty",expr("LineItem.ItemQty"))
      .withColumn("TotalValue",expr("LineItem.TotalValue"))
      .drop("LineItem")

      val invoiceWriterQuery = flatDF.writeStream
        .format("json")
        .option("path","output")
        .option("checkpointLocation","chk-point-dir")
        .outputMode(OutputMode.Append())
        .queryName("Flattened Invoices Writer")
        .trigger(Trigger.ProcessingTime(1L,TimeUnit.MINUTES))
        .start()


    logger.info("Job started")
    invoiceWriterQuery.awaitTermination()
  }

}
