package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types._

import java.util.concurrent.TimeUnit

object KafkaJsonSource {
  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .master("local[3]")
      .appName("KafkaJSONSourceApp")
      .config("spark.jars.packages", "org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0-preview2")
      .config("spark.streaming.stopGracefullyOnShutdown", "true")
      .getOrCreate()
    println(spark.conf.getAll.mkString("\n"))

    val valueDF = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", "invoices-ap")
      .option("startingOffsets", "earliest")
      .load()

    val schema = StructType(List(
      StructField("InvoiceNumber", StringType),
      StructField("CreatedTime", LongType),
      StructField("StoreID", StringType),
      StructField("PosID", StringType),
      StructField("CashierID", StringType),
      StructField("CustomerType", StringType),
      StructField("CustomerCardNo", StringType),
      StructField("TotalAmount", DoubleType),
      StructField("NumberOfItems", IntegerType),
      StructField("PaymentMethod", StringType),
      StructField("CGST", DoubleType),
      StructField("SGST", DoubleType),
      StructField("CESS", DoubleType),
      StructField("DeliveryType", StringType),
      StructField("DeliveryAddress", StructType(List(
        StructField("AddressLine", StringType),
        StructField("City", StringType),
        StructField("State", StringType),
        StructField("PinCode", StringType),
        StructField("ContactNumber", StringType)
      ))),
      StructField("InvoiceLineItems", ArrayType(StructType(List(
        StructField("ItemCode", StringType),
        StructField("ItemDescription", StringType),
        StructField("ItemPrice", DoubleType),
        StructField("ItemQty", IntegerType),
        StructField("TotalValue", DoubleType)
      )))),
    ))
    val invoiceDF = valueDF.select(from_json(col("value").cast("string"),schema).as("invoice"))

    val explodedInvoicesDF = invoiceDF.selectExpr("invoice.InvoiceNumber", "invoice.CreatedTime", "invoice.StoreID",
      "invoice.PosID", "invoice.CustomerType", "invoice.PaymentMethod", "invoice.DeliveryType", "invoice.DeliveryAddress.City",
      "invoice.DeliveryAddress.State", "invoice.DeliveryAddress.PinCode","explode(invoice.InvoiceLineItems) as LineItem")

    val lineItemDF = explodedInvoicesDF.withColumn("ItemCode",expr("LineItem.ItemCode"))
      .withColumn("ItemDescription",expr("LineItem.ItemDescription"))
      .withColumn("ItemPrice",expr("LineItem.ItemPrice"))
      .withColumn("ItemQty",expr("LineItem.ItemQty"))
      .withColumn("TotalValue",expr("LineItem.TotalValue"))
      .drop("LineItem")

    val notificationsDF = invoiceDF.selectExpr("invoice.InvoiceNumber as InvoiceNumber","invoice.CustomerCardNo as CustomerCardNo"
      ,"invoice.TotalAmount as TotalAmount", "invoice.TotalAmount * 0.2 as EarnedloyaltyPoints")


    val notificationDFKafka = notificationsDF.selectExpr("InvoiceNumber as key", "to_json(struct(*)) as value")


    //Sink-1 - file
    val query = lineItemDF.writeStream
      .format("json")
      .trigger(Trigger.ProcessingTime(1L, TimeUnit.MINUTES))
      .option("path", "output")
      .option("checkpointLocation", "chk-point-dir")
      .queryName("Kafka to JSON sink")
      .outputMode(OutputMode.Append())
      .start()

    //sink-2 Kafka topic
    notificationDFKafka.writeStream
      .format("kafka")
      .outputMode(OutputMode.Append())
      .queryName("Notification Writer")
      .option("checkpointLocation","notif-chk-point")
      .option("topic","notifications-ap")
      .option("kafka.bootstrap.servers","localhost:9092")
      .start()

    spark.streams.awaitAnyTermination()

  }

}
