package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, from_json, to_timestamp}
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

import java.util.concurrent.TimeUnit

object KafkaSourceCassandraSink extends Serializable {
  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Kafka source Cassandra Sink App")
      .master("local[3]")
      .config("spark.streaming.stopGracefullyOnShutdown", "true")
      .config("spark.sql.shuffle.partitions", 2)
      .config("spark.cassandra.connection.host", "104.197.146.183")
      .config("spark.cassandra.connection.port", "9042")
    //  .config("spark.cassandra.auth.username", "cassandra")
    //  .config("spark.cassandra.auth.password", "cassandra")
      .config("spark.sql.extensions", "com.datastax.spark.connector.CassandraSparkExtensions")
      .config("spark.sql.catalog.history", "com.datastax.spark.connector.datasource.CassandraCatalog")
      .getOrCreate()

    val loginSchema = StructType(List(StructField("created_time", StringType), StructField("login_id", StringType)))
    val kafkaSourceDF = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", "logins")
      .option("startingOffsets", "earliest")
      .load()
    val valueDF = kafkaSourceDF.select(from_json(col("value").cast("string"), loginSchema).as("value"))

    val loginDF = valueDF.select("value.*")
      .withColumn("created_time", to_timestamp(col("created_time"), "yyyy-MM-dd HH:mm:ss"))


    val userDF = spark.read
      .format("org.apache.spark.sql.cassandra")
      .option("keyspace", "amalks")
      .option("table", "users")
      .load()

    val joinExpr = loginDF.col("login_id") === userDF.col("login_id")

    val joinedDF = loginDF.join(userDF,joinExpr,"inner")
      .drop(loginDF.col("login_id"))

    val outputDF: DataFrame = joinedDF.select(col("login_id"),col("user_name"),col("created_time").as("last_login"))

    val streamingQuery = outputDF.writeStream.foreachBatch(printBatch _)
      .outputMode(OutputMode.Update())
      .option("checkpointLocation", "chk-point-dir")
      .trigger(Trigger.ProcessingTime(1, TimeUnit.MINUTES))
      .start()

    streamingQuery.awaitTermination()

  }

  def writeToCassandra(outputDF:DataFrame,batchId:Long):Unit = {
    outputDF.printSchema()
    outputDF.show()
        outputDF.write
          .format("org.apache.spark.sql.cassandra")
          .option("keyspace","amalks")
          .option("table","users")
          .mode("append")
          .save()

  }

  def printBatch(df: DataFrame, batchID:Long):Unit = {
    df.printSchema()
    df.show()
  }
}
