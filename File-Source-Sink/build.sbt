name := "spark-practice"

organization := "amal.example"

version := "0.1"

scalaVersion := "2.12.10"

//val sparkVersion = "3.0.0-preview2"
val sparkVersion = "2.4.7"


val sparkDependencies = Seq (
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-sql-kafka-0-10" % ,
  "com.datastax.spark" %% "spark-cassandra-connector" % "2.5.1",

)

val testDependencies = Seq ( "org.scalatest" %% "scalatest" % "3.0.8" % Test)

libraryDependencies ++= sparkDependencies ++ testDependencies


