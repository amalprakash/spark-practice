package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

object HelloRDD {

  @transient lazy val logger:Logger = Logger.getLogger(getClass.getName)
  def main(args: Array[String]):Unit = {

    if (args.length < 1)
      logger.error("Usage: HelloRDD filename ")

    val sparkConf = new SparkConf().setMaster("local[3]").setAppName("HelloRDD")
    val sparkContext = new SparkContext(sparkConf)
    val surveyRDD: RDD[Array[String]] = sparkContext.textFile(args(0))
      .repartition(2)
      .map(_.split(",").map(_.trim))
    val countRdd: RDD[(String, Int)] = surveyRDD.map(entry => SurveyRecord(entry(1).toInt, entry(2), entry(3), entry(4)))
      .filter(_.age < 40)
      .map(entry => (entry.country, 1))
      .reduceByKey((v1, v2) => v1 + v2)
    logger.info(countRdd.collect().mkString("->"))

    sparkContext.stop()

  }

}

case class SurveyRecord(age: Int, gender: String, country: String, state: String)
