package guru.learningjournal.spark.examples

import org.apache.spark.sql.SaveMode

object DataformatsDemo extends Serializable {

  import org.apache.log4j.Logger

  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main1(args:Array[String]): Unit = {
    import org.apache.spark.sql.SparkSession
    val spark = SparkSession.builder().appName("Data formats")
      .master("local[3]")
      .enableHiveSupport()
      .getOrCreate()

    val flightsCSV = spark.read.format("csv")
      .options(Map("header" -> "true", "inferSchema" -> "true"))
      .load("data/flight*.csv")

    val flightsJSON = spark.read.format("json")
      .load("data/flight*.json")

    val flightsParquet = spark.read.format("parquet")
      .load("data/flight*.parquet")

    flightsCSV.show(5)
    flightsJSON.show(5)
    flightsParquet.show(5)

    logger.info("CSV Schema: "+flightsCSV.schema.simpleString)
    logger.info("JSON Schema: "+flightsJSON.schema.simpleString)
    logger.info("Parquet Schema: "+flightsParquet.schema.simpleString)



    spark.stop()
  }
}

