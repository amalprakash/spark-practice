package amal.example.spark

import java.io.DataOutputStream
import java.net.{ServerSocket, Socket}
import java.util.Scanner

object NetcatServer {
  private var connectionSocket: Socket = _

  def main(args: Array[String]): Unit = {
    val port: Int = 9999
    var serverSocket: ServerSocket = new ServerSocket(port, 0)
    connectionSocket = serverSocket.accept()
    val outToClient: DataOutputStream = new DataOutputStream(connectionSocket.getOutputStream)
    try {
      while (true) {
        start(port, scala.io.StdIn.readLine(), outToClient)
      }
    } catch {
      case e:Exception => e.printStackTrace()
    } finally {
      outToClient.close()
      connectionSocket.close()
    }
  }

  private def start(port: Int, data: String, dos: DataOutputStream): Unit = {
    dos.writeBytes((new Scanner(data)).useDelimiter("\\n").next())
    }

}
