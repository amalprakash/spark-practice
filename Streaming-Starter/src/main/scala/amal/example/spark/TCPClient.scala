package amal.example.spark

import java.io.{BufferedReader, DataOutputStream, InputStreamReader}
import java.net.{ServerSocket, Socket}
import java.util.Scanner


object TCPClient {

  private var connectionSocket: Socket = _

  private var inFromClient: BufferedReader = _

  private def start(port: Int): Unit = {
    connectionSocket = null
    val serverSocket: ServerSocket = new ServerSocket(port, 0)
    var complete: Boolean = false
    do {
      if (connectionSocket == null) {
        connectionSocket = serverSocket.accept()
      }
      complete = if (System.in.available() > 0) download() else upload()
    } while (!complete);
    connectionSocket.close()
  }

  private def download(): Boolean = {
    val outToClient: DataOutputStream = new DataOutputStream(
      connectionSocket.getOutputStream)
    outToClient.writeBytes((new Scanner(scala.io.StdIn.readLine())).useDelimiter("\\Z").next())
    true
  }

  private def upload(): Boolean = {
    var complete: Boolean = false
    if (inFromClient == null) {
      inFromClient = new BufferedReader(
        new InputStreamReader(connectionSocket.getInputStream))
    }
    var line: String = null
    while ((line = inFromClient.readLine()) != null) {
      println(line)
      complete = true
    }
    complete
  }

  def main(args: Array[String]): Unit = {
    if (args(0) != null) {
      start(args(0).toInt)
    } else {
      println(
        "usage:\ndownload: java main.java.nc.NetcatServer [port] < [original-file]\nupload: java main.java.nc.NetcatServer [port] > [uploaded-file]")
    }
  }

}

