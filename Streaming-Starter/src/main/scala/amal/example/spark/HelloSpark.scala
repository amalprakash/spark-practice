package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, explode, split}
import org.apache.spark.sql.streaming.OutputMode

object HelloSpark {

  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("HelloSpark")
      .config("spark.streaming.stopGracefullyOnShutdown","true")
      .config("spark.sql.shuffle.partitions",3)
      .master("local[3]")
      .getOrCreate()

    val streamDF: DataFrame = spark.readStream.format("socket")
      .option("host","localhost")
      .option("port","9999")
      .load()

    val selectStream = streamDF.select(explode(split(col("value"), " ")).as("word"))
      .groupBy("word")
      .count()

    val wordStream = selectStream.writeStream
      .format("console")
      .option("checkpointLocation", "chk-point-dir")
      .outputMode(OutputMode.Complete())
      .start()

    wordStream.awaitTermination()
  }

}
