package amal.example.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.plans.logical.Join
import org.apache.spark.sql.functions.{coalesce, col, expr}

object HelloSpark {

  def main(args: Array[String]):Unit = {

    val spark = SparkSession
      .builder()
      .appName("JoinsApp")
      .master("local[3]")
      .getOrCreate()

    val salesDF = spark.read
      .options(Map("header"->"true","inferSchema"->"true"))
      .csv("data/1.csv")

    val productsDF = spark.read
      .options(Map("header"->"true","inferSchema"->"true"))
      .csv("data/2.csv")

    val prodRenamedDF = productsDF.withColumnRenamed("ProductId","pid")
      .withColumnRenamed("Description","descr")

    val joinSpec = salesDF.col("ProductId") === prodRenamedDF.col("pid")
    salesDF.join(prodRenamedDF,joinSpec,"full_outer")
      .drop("pid","descr")
      .withColumn("UnitPrice",coalesce(col("UnitPrice"),expr("TotalPrice/Qty")))
      .orderBy(col("OrderId"))
      .show()

    scala.io.StdIn.readLine()

    spark.stop()
  }

}
