package com.example.amal

import org.scalatest.{BeforeAndAfterAll, FunSuite}
import com.example.amal.RowExample.toDateFormat
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructType}

import java.sql.Date

class RowExampleTest extends FunSuite with BeforeAndAfterAll {

  @transient var spark:SparkSession = _
  @transient var myDF:DataFrame = _

  override protected def beforeAll(): Unit = {
    spark = SparkSession.builder().master("local[1]").getOrCreate()
    val myRows = Seq(Row("123","04/05/2010"),Row("124","02/05/2020"),Row("125","12/31/2020"))
    val mySchema = new StructType().add("id",StringType)
      .add("event_date",StringType)

    myDF = spark.createDataFrame(spark.sparkContext.parallelize(myRows),mySchema)
  }

  override protected def afterAll(): Unit = {
    spark.stop()
  }

  test("test data type") {
    val df2: Array[Row] = toDateFormat(myDF, "event_date", "M/d/y").collect()
    df2.foreach( row => assert(row.get(1).isInstanceOf[Date],"event_date must be a Date format"))
  }

  test("test data values")
  {

  }


}
