package com.example.amal

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

object RDDExample extends Serializable {

  def main(args: Array[String]): Unit = {

    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("MyRDDApp")

    val sparkContext = new SparkContext(sparkConf)

    val myRDD: RDD[String] = sparkContext.textFile("data/sample.csv")

    val myRDD2: RDD[Array[String]] = myRDD.map(line => line.split(",").map(_.trim))

    val myRDD3: RDD[(String, Int)] = myRDD2.map(record => SurveyRecord(record(1).toInt, record(2), record(3), record(4)))
      .map(sv => (sv.country, 1))
      .reduceByKey((v1, v2) => v1 + v2)


    println(myRDD3.collect().mkString("==>"))

  }

}

