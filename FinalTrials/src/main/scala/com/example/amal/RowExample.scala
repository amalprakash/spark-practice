package com.example.amal

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions.{col, to_date}
import org.apache.spark.sql.types.{StringType, StructType}

object RowExample extends Serializable {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().master("local[*]").getOrCreate()

    val myRows = Seq(Row("123","04/05/2010"),Row("124","02/05/2020"),Row("125","12/31/2020"))
    val mySchema = new StructType().add("id",StringType)
      .add("event_date",StringType)

    val myDF = spark.createDataFrame(spark.sparkContext.parallelize(myRows),mySchema)
    myDF.printSchema()
    val myDF2 = toDateFormat(myDF, "event_date", "M/d/y")

    myDF2.printSchema()
    myDF2.show()

     }

  def toDateFormat(df: DataFrame,colName:String, fmt:String):DataFrame = {
    df.withColumn(colName,to_date(col(colName),fmt))
  }

}
