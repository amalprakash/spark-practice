package com.example.amal

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction

import scala.util.matching.Regex

object UDFDemo extends Serializable {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().master("local[*]").getOrCreate()

    val df = spark.read
      .options(Map("inferSchema"->"true","header"->"true"))
      .csv("data/sample.csv")

    import org.apache.spark.sql.functions._

    val parseGender: UserDefinedFunction = udf(cleanGender(_: String): String)
    spark.udf.register("processGender",parseGender)

    val df2 = df.withColumn("new_gender",parseGender(col("gender")))
    df2.show()

    df2.createOrReplaceTempView("survey")
    spark.sql(
      """
        |select age,gender,country,processGender(gender) from survey
        |
        |""".stripMargin).show()

  }

  def cleanGender(gender:String):String = {
    val maleMatch:Regex = """^m$|^M$|^m.l.|^ma.|Ma.""".r
    val femaleMatch:Regex = """^f$|^F$|^F[a-z]*|^f[a-z]*|^w[a-z]*|^W[a-z]*""".r

    if(maleMatch.findFirstIn(gender).nonEmpty)
      "Male"
    else if (femaleMatch.findFirstIn(gender).nonEmpty)
      "Female"
      else
      "Unknown"

  }
}
