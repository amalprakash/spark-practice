package com.example.amal

import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

object MyDSExample extends Serializable {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().master("local[*]").appName("MyDSApp").getOrCreate()

    val schema = new StructType().add("age",IntegerType,true)
      .add("gender",StringType,true)
      .add("country",StringType,true)
      .add("state",StringType,true)

    val df: Dataset[Row] = spark.read.options(Map("header" -> "false", "nullValue"->"null")).schema(schema)
      .csv("data/sample.csv")

    df.show(3)
    import spark.implicits._
    val ds = df.select("age", "gender", "country", "state")
      .na.fill(0,Seq("age")).na.fill("Unknown",Seq("gender","country","state"))
      .as[SurveyRecord]

    ds.filter(row => row.age < 40).show()
  }

}

case class SurveyRecord(age: Int, gender: String, country: String, state: String)