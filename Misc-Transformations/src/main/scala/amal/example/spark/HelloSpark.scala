package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType

object HelloSpark {
  @transient lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[3]")
      .appName("MiscApp")
      .getOrCreate()

    val myList = List(("Amal", "15", "2", "1987"),
      ("John", "30", "4", "1990"),
      ("Adam", "1", "1", "89"),
      ("Elsa", "2", "7", "6"),
      ("Amal", "15", "2", "1987"))

    val myDF: DataFrame = spark.createDataFrame(myList).toDF("Name", "day", "month", "year")
    val myNewDF = myDF
      .withColumn("id", monotonically_increasing_id())
      .withColumn("day", col("day").cast(IntegerType))
      .withColumn("month", col("month").cast(IntegerType))
      .withColumn("year", col("year").cast(IntegerType))
      //      .withColumn("year", expr(
      //        """
      //          | case when year < 22 then cast(year as int) + 2000
      //          |       when year < 100 then cast(year as int) + 1900
      //          |       else year
      //          | end
      //          |""".stripMargin))
      .withColumn("year",
        when(col("year") < 22, col("year") + 2000)
          when(col("year") < 100, col("year") + 1900)
          otherwise (col("year")))


    myNewDF.show(truncate = false)
    myNewDF.printSchema()
  }

}
